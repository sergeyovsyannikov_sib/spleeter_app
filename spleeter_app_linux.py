from spleeter.audio import Codec
from spleeter.separator import Separator
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
import os

os.environ['CUDA_VISIBLE_DEVICES'] = "0"


class Ui_MainWindow(object):
    def __init__(self):
        self.audiofile = ''
        self.selectedcodec: Codec = Codec("wav")
        self.selectedseparator: Separator = Separator('spleeter:2stems')
        self.outputdestination = '/home/toddnach/SpleeterOutput/'

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(401, 220)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.status_text = QtWidgets.QLabel(self.centralwidget)
        self.status_text.setGeometry(QtCore.QRect(10, 150, 141, 31))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setWeight(75)
        self.status_text.setFont(font)
        self.status_text.setObjectName("status_text")

        self.status_state = QtWidgets.QLabel(self.centralwidget)
        self.status_state.setGeometry(QtCore.QRect(10, 180, 220, 31))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.status_state.setFont(font)
        self.status_state.setObjectName("status_state")

        self.sepBtn = QtWidgets.QPushButton(self.centralwidget)
        self.sepBtn.setGeometry(QtCore.QRect(290, 150, 101, 61))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setWeight(50)
        self.sepBtn.setFont(font)
        self.sepBtn.setObjectName("sepBtn")
        self.sepBtn.setEnabled(False)

        self.choosefBtn = QtWidgets.QPushButton(self.centralwidget)
        self.choosefBtn.setGeometry(QtCore.QRect(290, 80, 101, 61))
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setWeight(50)
        font.setKerning(True)
        self.choosefBtn.setFont(font)
        self.choosefBtn.setObjectName("choosefBtn")

        self.chooseoBtn = QtWidgets.QPushButton(self.centralwidget)
        self.chooseoBtn.setGeometry(QtCore.QRect(290, 10, 101, 61))
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setWeight(50)
        font.setKerning(True)
        self.chooseoBtn.setFont(font)
        self.chooseoBtn.setObjectName("choosefBtn")
        self.chooseoBtn.setEnabled(False)

        self.twoStemsButton = QtWidgets.QRadioButton(self.centralwidget)
        self.twoStemsButton.setEnabled(True)
        self.twoStemsButton.setGeometry(QtCore.QRect(10, 30, 161, 20))
        self.twoStemsButton.setCheckable(True)
        self.twoStemsButton.setChecked(True)
        self.twoStemsButton.setObjectName("twoBtn")

        self.stemsGroup = QtWidgets.QButtonGroup(MainWindow)
        self.stemsGroup.setObjectName("stemsGroup")
        self.stemsGroup.addButton(self.twoStemsButton)

        self.fourStemsButton = QtWidgets.QRadioButton(self.centralwidget)
        self.fourStemsButton.setGeometry(QtCore.QRect(10, 50, 231, 20))
        self.fourStemsButton.setObjectName("fourBtn")

        self.stemsGroup.addButton(self.fourStemsButton)

        self.choosesText = QtWidgets.QLabel(self.centralwidget)
        self.choosesText.setGeometry(QtCore.QRect(10, 10, 201, 16))
        self.choosesText.setObjectName("choosesText")
        self.choosecText = QtWidgets.QLabel(self.centralwidget)
        self.choosecText.setGeometry(QtCore.QRect(10, 80, 191, 16))
        self.choosecText.setObjectName("choosecText")

        self.wavButton = QtWidgets.QRadioButton(self.centralwidget)
        self.wavButton.setGeometry(QtCore.QRect(10, 100, 95, 20))
        self.wavButton.setChecked(True)
        self.wavButton.setObjectName("radioButton")

        self.codecGroup = QtWidgets.QButtonGroup(MainWindow)
        self.codecGroup.setObjectName("codecGroup")
        self.codecGroup.addButton(self.wavButton)

        self.mp3Button = QtWidgets.QRadioButton(self.centralwidget)
        self.mp3Button.setGeometry(QtCore.QRect(10, 120, 95, 20))
        self.mp3Button.setObjectName("radioButton_2")

        self.codecGroup.addButton(self.mp3Button)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Разделитель"))

        self.status_text.setText(_translate("MainWindow", "СТАТУС:"))
        self.status_state.setText(_translate("MainWindow", "Не разделено"))

        self.sepBtn.setText(_translate("MainWindow", "Разделить"))
        self.choosefBtn.setText(_translate("MainWindow", "Выбрать\n"
                                                         "аудио"))
        self.chooseoBtn.setText(_translate("MainWindow", "Выбрать папку\n"
                                                         "для результата"))
        self.twoStemsButton.setText(_translate("MainWindow", "Вокал и инструментал"))
        self.fourStemsButton.setText(_translate("MainWindow", "Вокал, барабаны, бас и остальное"))

        self.choosesText.setText(_translate("MainWindow", "Выбрать желаемое разделение:"))
        self.choosecText.setText(_translate("MainWindow", "Выбрать кодек итоговых аудио:"))

        self.wavButton.setText(_translate("MainWindow", "WAV"))
        self.mp3Button.setText(_translate("MainWindow", "MP3"))

    def selectcodec(self, codecname):
        self.selectedcodec = Codec(codecname)

    def selectseparator(self, separatorname):
        self.selectedseparator = Separator(separatorname)

    def selectfile(self):
        _translate = QtCore.QCoreApplication.translate
        self.status_state.setText(_translate("MainWindow", "Не разделено"))
        self.audiofile = QFileDialog.getOpenFileName(None, 'Выберите аудио', 'C:/', "Аудио (*.mp3 *.wav *.m4a)")[0]
        self.sepBtn.setEnabled(True)

    def selectoutput(self):
        self.outputdestination = QFileDialog.getExistingDirectory(None, 'Выберите папку', 'C:/')[0]

    def separating(self):
        _translate = QtCore.QCoreApplication.translate
        separator = self.selectedseparator
        separator.separate_to_file(self.audiofile, self.outputdestination, codec=self.selectedcodec)
        self.status_state.setText(_translate("MainWindow", "Операция завершена"))
        self.sepBtn.setEnabled(False)

    def application(self):
        _translate = QtCore.QCoreApplication.translate
        self.mp3Button.clicked.connect(lambda: self.selectcodec("mp3"))
        self.wavButton.clicked.connect(lambda: self.selectcodec("wav"))
        self.fourStemsButton.clicked.connect(lambda: self.selectseparator('spleeter:4stems'))
        self.twoStemsButton.clicked.connect(lambda: self.selectseparator('spleeter:2stems'))
        self.chooseoBtn.clicked.connect(lambda: self.selectoutput())
        self.choosefBtn.clicked.connect(lambda: self.selectfile())
        self.sepBtn.clicked.connect(lambda: self.separating())


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    ui.application()
    sys.exit(app.exec_())
